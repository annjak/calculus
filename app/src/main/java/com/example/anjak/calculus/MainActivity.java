package com.example.anjak.calculus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    TextView result;
    EditText num1;
    EditText num2;

    Button btnAdd, btnSubtract, btnMultiply, btnDevide, btnAC;

    double result_num;
    double nummer1;
    double nummer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = (TextView) findViewById(R.id.Result);
        num1 = (EditText) findViewById(R.id.number1);
        num2 = (EditText) findViewById(R.id.number2);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSubtract = (Button) findViewById(R.id.btnSubtract);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnDevide = (Button) findViewById(R.id.btnDevide);
        btnAC = (Button) findViewById(R.id.btnAC);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (num1.getText() != null && num2.getText() != null) {

                    nummer1 = Double.parseDouble(num1.getText().toString());
                    nummer2 = Double.parseDouble(num2.getText().toString());

                    result_num = nummer1 + nummer2;

                    result.setText(String.valueOf(result_num));
                } else {

                    Toast.makeText(v.getContext(), "Fel inmatning", Toast.LENGTH_SHORT).show();

                }


            }


        });


        btnSubtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (num1.getText() != null && num2.getText() != null) {

                    nummer1 = Double.parseDouble(num1.getText().toString());
                    nummer2 = Double.parseDouble(num2.getText().toString());

                    result_num = nummer1 - nummer2;
                } else {
                    Toast.makeText(v.getContext(), "Fel inmatning", Toast.LENGTH_SHORT).show();

                }

                result.setText(String.valueOf(result_num));
            }


        });

        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText() != null && num2.getText() != null) {

                    nummer1 = Double.parseDouble(num1.getText().toString());
                    nummer2 = Double.parseDouble(num2.getText().toString());

                    result_num = nummer1 * nummer2;
                    } else {
                    Toast.makeText(v.getContext(), "Fel inmatning", Toast.LENGTH_SHORT).show();

                    }


                                               result.setText(String.valueOf(result_num));
                                           }
                                       }

        );

        btnDevide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (num1.getText() != null && num2.getText() != null) {


                    nummer1 = Double.parseDouble(num1.getText().toString());
                    nummer2 = Double.parseDouble(num2.getText().toString());

                    result_num = nummer1 / nummer2;
                } else {
                    Toast.makeText(v.getContext(), "Fel inmatning", Toast.LENGTH_SHORT).show();
                }
                result.setText(String.valueOf(result_num));

            }
        });

       btnAC.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                result.setText("");
                num1.setText("");
                num2.setText("");

                }


        });

    }
}